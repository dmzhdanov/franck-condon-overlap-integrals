# Franck-Condon overlap integrals #

c++ and Mathematica code to calculate the Franck-Condon overlap integrals between the states of two different harmonic oscillators.

Output is the matrix with ij-elements equal to overlap <i|j> between i-th state of the 1-st oscillator and j-nd state of the second one.

### Dependencies ###

* c++ implementation requires [Eigen 3.3](https://eigen.tuxfamily.org/dox/) linear algebra template library.

### References ###

* The algorithm is gathered from:  
Per-Ake Malmqvist, Niclas Forsberg,
[*"Franck-Condon factors for multidimensional harmonic oscillators"*](http://www.sciencedirect.com/science/article/pii/S0301010497003479), 
Chemical Physics, 228, 227–240 (1998).