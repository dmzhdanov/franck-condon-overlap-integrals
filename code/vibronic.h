#ifndef VIBRONIC_H
#define VIBRONIC_H
#include <cmath>
#include <Eigen/Dense>
#include <Eigen/Sparse>

namespace harmonic
{
	template<class T>
	inline void FranckCondon(Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic,Eigen::RowMajor,-1,-1>* output,
							int N1 /*number of levels to consider in 1-st oscillator*/, double mass1, double frequency1/*cyclic*/, double x_eq1/*equilibrium coordinate*/, 
							int N2 /*number of levels to consider in 2-nd oscillator*/, double mass2, double frequency2/*cyclic*/, double x_eq2/*equilibrium coordinate*/,
							double hbar=1)
	{
	/*!
	* calculates the matrix of Franck-Condon overlap integrals.
	* \tparam T - (template parameter) output storage data type.
	* \tparam output - output matrix with ij-elements equal to overlap <i|j> between i-th state of the 1-st oscillator and j-nd state of the second one.
	* \tparam N1 - number of levels to consider in the 1-st harmonic oscillator.
	* \tparam mass1 - mass of the 1-st harmonic oscillator.
	* \tparam frequency1 - frequency of the 1-st harmonic oscillator.
	* \tparam x_eq1 - equilibrium coordinate for the 1-st harmonic oscillator.
	*
	* The algorithm is gathered from: 
	* Per-Ake Malmqvist, Niclas Forsberg,
	*	"Franck-Condon factors for multidimensional harmonic oscillators", 
	*	Chemical Physics, 228, 227�240 (1998).
	*/
		//computing L and U matrices
		double C,C1,C2;
		C1=std::sqrt(mass1*frequency1/hbar);
		C2=std::sqrt(mass2*frequency2/hbar);
		C=std::sqrt((C1*C1+C2*C2)/2);
        //double x_eq=(x_eq1*C1*C1+x_eq2*C2*C2)/(2*C*C);

        //int dim=std::max(N1,N2);
		int dim_min=std::min(N1,N2);
		int dimL=std::max(N1,dim_min);
		int dimU=std::max(N2,dim_min);
		using MatrixXc_r = Eigen::Matrix<T,Eigen::Dynamic,Eigen::Dynamic,Eigen::RowMajor,-1,-1> ;
		MatrixXc_r L, U;
		L=MatrixXc_r::Zero(dimL,dimL);
		U=MatrixXc_r::Zero(dimU,dimU);
		L(0,0)=1;
		U(0,0)=1;
		double cl_1=C1/C;
		double cl_2=C1*C2*C2/(std::sqrt(2)*C*C)*(x_eq1-x_eq2);
		double cl_3=(C1*C1-C2*C2)/(2*C1*C);

		double cu_1=C2/C;
		double cu_2=C2*C1*C1/(std::sqrt(2)*C*C)*(x_eq2-x_eq1);
		double cu_3=(C2*C2-C1*C1)/(2*C2*C);

		double coeff1=mass1*mass2*frequency1*frequency2;
		double coeff2=mass1*frequency1+mass2*frequency2;
		double FCF00= std::pow(coeff1,0.25)/(std::sqrt(coeff2/2))*std::exp(-coeff1*std::pow(x_eq1-x_eq2,2)/(2*hbar*coeff2));

		int m,t;
		for(m=0;m<dimL-1;m++)
		{
			for(t=0;t<=m+1;t++)
			{
				L(m+1,t)=1/std::sqrt(m+1)* \
					( \
						(t>0 ? cl_1*std::sqrt(t)*L(m,t-1) : 0)- \
						cl_2*L(m,t)+ \
						(t<dimL-1 ? cl_3*std::sqrt(t+1)*L(m,t+1) : 0) \
					);
			};
		};
		for(m=0;m<dimU-1;m++)
		{
			for(t=0;t<=m+1;t++)
			{
				U(t,m+1)=1/std::sqrt(m+1)* \
					( \
						(t>0 ? cu_1*std::sqrt(t)*U(t-1,m) : 0)- \
						cu_2*U(t,m)+ \
						(t<dimU-1 ? cu_3*std::sqrt(t+1)*U(t+1,m) : 0) \
					);
			};
		};
		output->resize(N1,N2);
        output[0].noalias()=((L.block(0,0,N1,dim_min))*(U.block(0,0,dim_min,N2))*FCF00).template cast<T>();
	};

	template<class T>
    inline void Creation(Eigen::SparseMatrix<T,Eigen::RowMajor>* output, int NOfLevels, int /*startLevel=0*/, T multiplier=1)//,bool overwrite=false)
	{
		output->conservativeResize(NOfLevels,NOfLevels);
		for (int i = 1; i < NOfLevels; i++)
		{
			output->coeffRef(i,i-1)+=(T)(std::sqrt(i)*multiplier);
		};
	};

	template<class T>
    inline void Annihilation(Eigen::SparseMatrix<T,Eigen::RowMajor>* output, int NOfLevels, int /*startLevel=0*/, T multiplier=1)
	{
		output->conservativeResize(NOfLevels,NOfLevels);
		for (int i = 1; i < NOfLevels; i++)
		{
			output->coeffRef(i-1,i)+=(T)(std::sqrt(i)*multiplier);
		};
	};

	template<class T>
	inline void Coordinate(Eigen::SparseMatrix<T,Eigen::RowMajor>* output,
							int N1, double mass, double frequency/*cyclic*/, int startLevel=0, double hbar=1, T multiplier=1)
	{
		T mult=multiplier*std::sqrt(hbar/2/mass/frequency);
		Creation(output,N1,startLevel,mult);
		Annihilation(output,N1,startLevel,mult);
	};

	inline void Momentum(Eigen::SparseMatrix<std::complex<double>,Eigen::RowMajor>* output,
							int N1, double mass, double frequency/*cyclic*/, int startLevel=0, double hbar=1, std::complex<double> multiplier=1)
	{
		std::complex<double> mult=multiplier*std::complex<double>(0,1)*std::sqrt(mass*frequency*hbar/2);
		Creation(output,N1,startLevel,mult);
		Annihilation(output,N1,startLevel,-mult);
	};	
};
#endif
